﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProgressBar;

public class GazeController : MonoBehaviour
{
    public ProgressRadialBehaviour helpFiller;
    public ProgressRadialBehaviour exitFiller;

    private List<ProgressRadialBehaviour> fillers = new List<ProgressRadialBehaviour>();



    private void Awake()
    {
        fillers.Add(helpFiller);
        fillers.Add(exitFiller);

    }

    private void Update()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, 500f))
        {


            if (hit.collider.CompareTag("Button"))
            {
                ActivateCard(hit.collider.name);
            }
        }

        else
        {
            for (int i = 0; i != fillers.Count; i++)
            {
                if (fillers[i].Value > 0)
                {
                    fillers[i].DecrementValue(1f);
                }
            }
        }
    }

    void ActivateCard(string name)
    {

        if (name == "Help")
        {
            helpFiller.IncrementValue(1f);
        }

        else if (name == "Exit")
        {
            exitFiller.IncrementValue(1f);
        }

    }

}
