﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyController : MonoBehaviour
{
    Rigidbody m_RigidBody;
    Vector3 m_TransformPosition;

    public GameObject ExtenguisherPrefab;
    // Start is called before the first frame update
    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody>();
        m_TransformPosition = gameObject.transform.position;
    }


    private void OnTriggerEnter(Collider other)
    {
        m_RigidBody.useGravity = false;
        gameObject.transform.position = m_TransformPosition;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
