﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{

    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void ShutOff()
    {
        StartCoroutine(StartShutOff());
    }

    IEnumerator StartShutOff()
    {
        anim.SetBool("goback", true);
        yield return new WaitForSeconds(0.5f);

        anim.SetBool("goback", false);
        this.gameObject.SetActive(false);
    }
}
