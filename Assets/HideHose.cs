﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideHose : MonoBehaviour
{
    public GameObject Hose;

    Vector3 m_Transform;

    private void Start()
    {
        m_Transform = this.transform.position;    
    }
    public void Hide()
    {
        Hose.SetActive(false);
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

    }

    public void Reset()
    {
        this.transform.position = m_Transform;
        Hose.SetActive(true);
    }
}
