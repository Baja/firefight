﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class DrewInteract : MonoBehaviour
{
    [HideInInspector]
    public Hand m_ActiveHand = null;

    public GameObject ThrowFire;

    // Start is called before the first frame update
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Water")
        {
            ThrowFire.SetActive(true);
        }
    }
}
